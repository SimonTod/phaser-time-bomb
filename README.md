# PHASER TIME BOMB 💣

## How to contribute

Clone project

Install dependencies

`npm install`

Launch project

`npm run dev`

Project should now be accessible with browser at https://localhost:9000/

## GIT COMMANDS

### create new branch

`git branch {new branch name} master`
`git checkout {new branch name}` (nom de la branche précédemment créée)

faire des modifs

### commit mes modifications

`git add .`
`git commit -m '{titre de mon commit}'`

### Pousser mes commit sur le serveur

`git push origin {new branch name}` (nom de la branche précédemment créée)